package com.example.tim15;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Contact;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ContactService;
import com.example.tim15.Servis.ServiceClass;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateContactActivity extends AppCompatActivity {

    private static int TIME_OUT = 2000;
    private EditText ime;
    private EditText prezime;
    private EditText email;
    private EditText displayName;
    private EditText note;
    public static List<String> kontaktiZaProveru = new ArrayList<>();
    public String activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Intent intent =getIntent();
        Contact contact =(Contact) intent.getSerializableExtra("contact");

        activity = getIntent().getStringExtra("FromActivitySwap");

        if(activity.equals("ContactActivity")) {
            setTitle("Izmena kontakta");
        }else if(activity.equals("ContactsActivity")){
            setTitle("Dodavanje kontakta");
        }

        ime = (EditText) findViewById(R.id.editText10);
        prezime = (EditText) findViewById(R.id.editText11);
        email = (EditText) findViewById(R.id.editText12);
        displayName = (EditText) findViewById(R.id.editText3);
        note = (EditText) findViewById(R.id.editText7);

        if (activity.equals("ContactActivity")){
            ime.setText(contact.getFirstName());
            prezime.setText(contact.getLastName());
            displayName.setText(contact.getDisplay());
            note.setText(contact.getNote());
            email.setText(contact.getEmail());
            email.setEnabled(false);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_contact, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(CreateContactActivity.this,SettingsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.action_save) {

            if (activity.equals("ContactsActivity")) {

                String imeText = ime.getText().toString().trim();
                String prezimeText = prezime.getText().toString().trim();
                String emailText = email.getText().toString().trim();
                String displayText = displayName.getText().toString().trim();
                String noteText = note.getText().toString().trim();

                    final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
                    final String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

                    Contact contact = new Contact();
                    contact.setFirstName(imeText);
                    contact.setLastName(prezimeText);
                    contact.setEmail(emailText);
                    contact.setDisplay(displayText);
                    contact.setNote(noteText);

                    kreiranjeKontakta(contact, ipAdrs);

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.createContact), "Kreiran kontakt!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(CreateContactActivity.this, ContactsActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME_OUT);

            }
            else if (activity.equals("ContactActivity")){


                String imeText = ime.getText().toString().trim();
                String prezimeText = prezime.getText().toString().trim();
                String emailText = email.getText().toString().trim();
                String displayText = displayName.getText().toString().trim();
                String noteText = note.getText().toString().trim();



                    Intent intent =getIntent();
                    Contact contact1 =(Contact) intent.getSerializableExtra("contact");

                final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
                final String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

                Toast.makeText(this, ipAdrs, Toast.LENGTH_SHORT).show();

                    Contact contact = new Contact();
                    contact.setId(contact1.getId());
                    contact.setDisplay(displayText);
                    contact.setEmail(contact1.getEmail());
                    contact.setFirstName(imeText);
                    contact.setLastName(prezimeText);
                    contact.setNote(noteText);

                    izmenaKontakta(contact,ipAdrs);

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.createContact), "Kontakt izmenjen!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(CreateContactActivity.this, ContactsActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME_OUT);

         }
        }



        if (id == R.id.action_cancel) {
            Intent intent = new Intent(CreateContactActivity.this, ContactsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void kreiranjeKontakta(Contact createContact, String username){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        ContactService contactService1 = retrofit1.create(ContactService.class);
        Call<Contact> call = contactService1.createContact(createContact, username);

        call.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                //nista
            }
        });
    }

    public void izmenaKontakta(Contact izmena,String username){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        ContactService contactService = retrofit1.create(ContactService.class);
        Call<Contact> call = contactService.updateContact(izmena,izmena.getId(),username);

        call.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> contact) {
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                //nista
            }
        });
    }



    public boolean validacija1(String imeText, String prezimeText, String emailText) {
        if (imeText.equals("") || prezimeText.equals("") || emailText.equals("")) {
            android.widget.Toast.makeText(getApplicationContext(), "Morate uneti sva polja!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean validacija2(String imeText, String prezimeText, String emailText) {
        if (!Pattern.matches("[a-zA-Z]+",imeText)) {
            android.widget.Toast.makeText(getApplicationContext(), "Ime mora sadrzati iskljucivo slova i ne sme imati razmak!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Pattern.matches("[a-zA-Z]+",prezimeText)){
            android.widget.Toast.makeText(getApplicationContext(), "Prezime mora sadrzati iskljucivo slova i ne sme imati razmak!", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(emailText).matches()){
            android.widget.Toast.makeText(getApplicationContext(), "Email je u neispravnom formatu!", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            return true;
        }
    }
    public boolean validacija3(String emailText){
            if (kontaktiZaProveru.contains(emailText)){
                android.widget.Toast.makeText(getApplicationContext(), "Vec postoji kontakt sa ovim emailom!", Toast.LENGTH_SHORT).show();
                return false;
            }
            else {
                return true;
            }
    }





    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}

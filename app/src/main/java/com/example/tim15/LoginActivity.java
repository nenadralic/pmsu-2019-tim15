package com.example.tim15;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ServiceClass;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity  implements View.OnClickListener {

    private EditText username;
    private EditText password;
    private String T="Neispravan Username ili Password";
    private String B="Niste uneli Username ili Password";
    private Button btnStartEmail;
    public static List<Account> nalozi = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    public static final String Pref = "Pref";
    public static final String Username = "username";
    public String activity;
    private AccountService accountService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        activity = getIntent().getStringExtra("FromActivityChange");

        username = (EditText)findViewById(R.id.editText);
        password = (EditText)findViewById(R.id.editText2);
        btnStartEmail = (Button)findViewById(R.id.btn_start_email);

        btnStartEmail.setOnClickListener(this);

        sharedPreferences = getSharedPreferences(Pref, Context.MODE_PRIVATE);
    }
    @Override
    public void onBackPressed() {
        if (activity == null) {
            System.exit(0);
        }
        else if (activity.equals("EmailsActivity1") || activity.equals("ContactsActivity1")
                || activity.equals("FoldersActivity1") || activity.equals("ProfileActivity1") || activity.equals("IzmenaProfilaActivity")){
            //nista
        }
    }


    @Override
    public void onClick(View view) {
        //android.widget.Toast.makeText(getApplicationContext(), "Kliknuo sam", Toast.LENGTH_SHORT).show();
        String user = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        accountService = ServiceClass.accountService;

        Call<Account> call1 = accountService.login(user,pass);

        call1.enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Call<Account> call, Response<Account> response) {
                if (response.isSuccessful()) {
                    Account account = response.body();

                    String user1 = username.getText().toString().trim();
                    String pass1 = password.getText().toString().trim();



                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    if (!(user1.equals("") || pass1.equals(""))) {

                        int br=0;
                            if(user1.equals(account.getUsername()) && pass1.equals(account.getPassword())){

                                editor.putString(Username, account.getUsername());
                                editor.commit();

                                String activity1 = getIntent().getStringExtra("FromActivityChange");

                                if (activity1 != null) {
                                    if ((!activity1.equals("EmailsActivity1") && !activity1.equals("ContactsActivity1")
                                            && !activity1.equals("FoldersActivity1") && !activity1.equals("ProfileActivity1"))) {

                                        if (activity1.equals("EmailsActivity") || activity1.equals("IzmenaProfilaActivity")) {
                                            Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                                            startActivity(intent);
                                            br++;
                                        } else if (activity1.equals("FoldersActivity")) {
                                            //Intent intent = new Intent(LoginActivity.this, FoldersActivity.class);
                                            //startActivity(intent);
                                            br++;
                                        } else if (activity1.equals("ContactsActivity")) {
                                            //Intent intent = new Intent(LoginActivity.this, ContactsActivity.class);
                                            //startActivity(intent);
                                            br++;
                                        }
                                        else if (activity1.equals("ProfileActivity")) {
                                            //Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                                            //startActivity(intent);
                                            br++;
                                        }
                                    }else {
                                        Intent intent  = new Intent(LoginActivity.this,EmailsActivity.class);
                                        startActivity(intent);
                                        br++;
                                    }
                                }
                                else if (activity1 == null || activity1.equals("")){
                                    Intent intent  = new Intent(LoginActivity.this,EmailsActivity.class);
                                    android.widget.Toast.makeText(getApplicationContext(), "Dobar login", Toast.LENGTH_SHORT).show();
                                    startActivity(intent);
                                    br++;
                                }
                            }

                        if(br==0) {
                            android.widget.Toast.makeText(getApplicationContext(), T, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        android.widget.Toast.makeText(getApplicationContext(), B, Toast.LENGTH_LONG).show();
                        editor.clear().commit();
                        username.setText("");
                        password.setText("");
                    }

                }

            }

            @Override
            public void onFailure(Call<Account> call, Throwable t) {
                Log.d("Greska: ", t.getMessage());
            }
        });



    }
    public void registerKlik(View v)
    {
        Intent intent  = new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}

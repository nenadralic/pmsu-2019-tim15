package com.example.tim15;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Email;
import com.example.tim15.Model.Folder;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.EmailService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FolderActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private String T="Kreiranje novog mejla";
    public static ListView list;

    public static List<Email> emailovi1= new ArrayList<>();
    public static List<Account> nalozi = new ArrayList<>();
    private static int TIME_OUT = 2000;
    private SharedPreferences sharedPreferences;
    private String userPreferences;
    private String izborPref;
    private String sinhronizacijaPref;
    private long interval;
    private Handler handler;
    private EmailService emailsService;
    private AccountService accountService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Poruke u folderu");
        sharedPreferences = getSharedPreferences(LoginActivity.Pref, Context.MODE_PRIVATE);
        userPreferences = sharedPreferences.getString(LoginActivity.Username,"");

        Intent intent = getIntent();
        final Folder folder =(Folder) intent.getSerializableExtra("folder");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        izborPref = sharedPreferences.getString("list_preference_2", "1");

        sinhronizacijaPref = sharedPreferences.getString("list_preference_1", "1");

        interval = TimeUnit.MINUTES.toMillis(Integer.parseInt(sinhronizacijaPref));

        fab = (FloatingActionButton)findViewById(R.id.floatingActionButton);

        list= findViewById(R.id.emailsView);

      //  list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

        Call call = emailsService.getEmailsFolder(userPreferences, folder.getName());

        call.enqueue(new Callback<List<Email>>() {
            @Override
            public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                list = findViewById(R.id.listaEmailova);
                emailovi1 = response.body();
                list.setAdapter(new FolderActivity.EmailsAdapter(FolderActivity.this, R.layout.row_emails,emailovi1));

            }
            @Override
            public void onFailure (Call < List < Email >> call, Throwable t){
                Toast.makeText(FolderActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
            }

        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Email emailPosiljaoca = new Email();
                emailPosiljaoca = (Email) (list.getItemAtPosition(position));
                String emailPosiljaocaText = emailPosiljaoca.getFrom();
                Date datum = emailPosiljaoca.getDateTime();
                //Toast.makeText(EmailsActivity.this, String.valueOf(emailovi1.size()), Toast.LENGTH_SHORT).show();
                for (Email email : emailovi1) {
                    //Toast.makeText(EmailsActivity.this, stringDatum(email.getDateTime()), Toast.LENGTH_SHORT).show();
                    if (emailPosiljaoca.equals(email)) {
                        //if (stringDatum(datum) == stringDatum(email.getDateTime())) {
                        Email emailSend = email;
                        Intent intent = new Intent(view.getContext(), EmailActivity.class);
                        //emailSend.setProcitano(true);

                    //    izmenaEmaila(emailSend.getId());//
                        intent.putExtra("email", emailSend);
                        startActivity(intent);
                        break;
                        //}
                        //else { Toast.makeText(EmailsActivity.this, "Nije proslo if za datum", Toast.LENGTH_SHORT).show(); }


                    }
                    else { Toast.makeText(FolderActivity.this, "Nije proslo if", Toast.LENGTH_SHORT).show(); }
                }
            }
        });

    }

    public class EmailsAdapter extends ArrayAdapter<Email> {
        private Context mContext;
        int mResource;

        public EmailsAdapter(Context context, int resource, List<Email> objects) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
        }



        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            String email = getItem(position).getFrom();
//            String prezime = getItem(position).getFrom().getLastName();

            String naslov = getItem(position).getSubject();
            String poruka = getItem(position).getContent();
          //  String datum = stringDatum(getItem(position).getDateTime());//
            String emailPosiljaoca = getItem(position).getFrom();
            boolean procitan = getItem(position).isProcitano();

            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            if(procitan==false) {
                ImageView image = convertView.findViewById(R.id.imageEmails);
                TextView xfrom = convertView.findViewById(R.id.fromTXT);
                TextView xnaslov = convertView.findViewById(R.id.naslovEmails);
                TextView xporuka = convertView.findViewById(R.id.porukaEmails);
                TextView xdatum = convertView.findViewById(R.id.datumEmails);
                TextView xemailPosiljaoca = convertView.findViewById(R.id.email);
                TextView xnova = convertView.findViewById(R.id.newMessage);

                xnova.setVisibility(View.VISIBLE);
                xporuka.setTextColor(Color.parseColor("#ff0099cc"));
                xporuka.setTypeface(null, Typeface.BOLD_ITALIC);

                image.setImageResource(R.drawable.pismo);
//                imePrezime.setText(ime + " " + prezime);
                xnaslov.setText(naslov);
                xporuka.setText(poruka);
          //      xdatum.setText(datum);
                xfrom.setText(email);
                xemailPosiljaoca.setText(emailPosiljaoca);

                return convertView;

            }else if(procitan==true){
                ImageView image = convertView.findViewById(R.id.imageEmails);
                TextView xfrom= convertView.findViewById(R.id.fromTXT);
                TextView xnaslov = convertView.findViewById(R.id.naslovEmails);
                TextView xporuka = convertView.findViewById(R.id.porukaEmails);
                TextView xdatum = convertView.findViewById(R.id.datumEmails);
                TextView xemailPosiljaoca = convertView.findViewById(R.id.email);

                image.setImageResource(R.drawable.pismo);
//                imePrezime.setText(ime + " " + prezime);
                xnaslov.setText(naslov);
                xporuka.setText(poruka);
    //            xdatum.setText(datum);
                xfrom.setText(email);
                xemailPosiljaoca.setText(emailPosiljaoca);

                return convertView;

            }
            return null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
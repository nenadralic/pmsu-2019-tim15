package com.example.tim15.Servis;

import com.example.tim15.Model.Email;
import com.example.tim15.Model.SimpleQuery;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EmailService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET(ServiceClass.MESSAGESASC)
    Call<List<Email>> getEmailsAsc(@Path("username") String username);

    @GET(ServiceClass.MESSAGESDES)
    Call<List<Email>> getEmailsDes(@Path("username") String username);

    @GET(ServiceClass.EMAILSFOLDER)
    Call<List<Email>> getEmailsFolder(@Path("username") String username , @Path("name") String name);

    @PUT(ServiceClass.EMAILVIEW)
    Call<Void> viewEmail(@Path("id") int id);

    @POST(ServiceClass.EMAILSADD)
    Call<Email> createEmail(@Body Email email, @Path("username") String username);

    @DELETE(ServiceClass.EMAILDELETE)
    Call<Void> deleteEmail(@Path("id") int id);

    @POST(ServiceClass.EMAILSEARCHTERM)
    Call<List<Email>> searchEmailTerm(@Body SimpleQuery query);

    @POST(ServiceClass.EMAILSEARCHPHRASE)
    Call<List<Email>> searchEmailPhrase(@Body SimpleQuery query);

    @GET(ServiceClass.REINDEXEMAILS)
    Call<Void> reindexEmails();

/*    @PUT(ServiceClass.EMAILUPDATE)
    Call<Email> updateEmail(@Path("id") int id, @Body Email email);*/

}

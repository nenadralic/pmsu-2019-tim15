package com.example.tim15.Servis;

import com.example.tim15.Model.Contact;
import com.example.tim15.Model.SimpleQuery;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET(ServiceClass.CONTACTSUSER)
    Call<List<Contact>> getContacts(@Path("username") String username);

    @POST(ServiceClass.CONTACTADD)
    Call<Contact> createContact(@Body Contact contact, @Path("username") String username);

    @DELETE(ServiceClass.CONTACTSDELETE)
    Call<Void> deleteContact(@Path("id") int id);

    @PUT(ServiceClass.CONTACTUPDATE)
    Call<Contact> updateContact(@Body Contact contact,
                                @Path("id") Integer id,@Path("username") String username);

    @POST(ServiceClass.CONTACTSEARCHTERM)
    Call<List<Contact>> searchContactTerm(@Body SimpleQuery query);

    @POST(ServiceClass.CONTACTSEARCHPHRASE)
    Call<List<Contact>> searchContactPhrase(@Body SimpleQuery query);

    @GET(ServiceClass.REINDEXCONTACTS)
    Call<Void> reindexContacts();
}

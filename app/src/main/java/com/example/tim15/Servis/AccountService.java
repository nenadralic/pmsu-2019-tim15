package com.example.tim15.Servis;

import com.example.tim15.Model.Account;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AccountService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET(ServiceClass.ACCOUNTSUSERNAME)
    Call<List<Account>> getAccounts(@Path("username") String username);

    @PUT(ServiceClass.ACCOUNTUPDATE)
    Call<Account> updateAccount(@Path("id") int id, @Body Account account);

    @PUT(ServiceClass.ACCOUNTADD)
    Call<Void> addAccount(@Body Account account ,@Path("username") String username);

    @POST(ServiceClass.USERADD)
    Call<Void> registrationUser(@Path("username") String username, @Path("password") String password, @Path("name") String name, @Path("lastname") String lastname);

    @PUT("accounts/{id}")
    Call<Account> editAccount(@Body Account account, @Path("id") int id);


    @GET("accounts/username/{username}")
    Call<Account> getAccount(@Path("username") String username);

    @GET(ServiceClass.LOGIN)
    Call<Account> login(@Path("username") String username, @Path("password") String password);


}

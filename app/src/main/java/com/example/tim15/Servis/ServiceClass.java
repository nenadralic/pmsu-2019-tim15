package com.example.tim15.Servis;

import android.support.design.widget.Snackbar;

import com.example.tim15.Model.Contact;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceClass {


    public static final String SERVICE_API_PATH = "http://192.168.0.100:8080/";
    //search
    public static final String EMAILSEARCHTERM = "search/message/term";
    public static final String EMAILSEARCHPHRASE = "search/message/phrase";
    public static final String CONTACTSEARCHTERM = "search/contact/term";
    public static final String CONTACTSEARCHPHRASE = "search/contact/phrase";

    public static final String REINDEXEMAILS = "index/reindex-emails";
    public static final String REINDEXCONTACTS = "index/reindex-contacts";
    //messages
    public static final String MESSAGESASC = "messages/account/sortAsc/{username}";
    public static final String MESSAGESDES = "messages/account/sortDes/{username}";
    public static final String MESSAGES = "messages";
    public static final String EMAILVIEW = "messages/viewed/{id}";
    public static final String EMAILDELETE = "messages/delete/{id}";
    public static final String EMAILSADD = "messages/send/{username}";
    public static final String EMAILSFOLDER = "messages/{username}/{name}";
    //LoginRegister
    public static final String LOGIN = "accounts/login/{username}/{password}";
    public static final String ACCOUNTADD = "accounts/addAccount/{username}";
    public static final String USERADD ="accounts/registrationUser/{username}/{password}/{name}/{lastname}";
    //folder
    public static final String FOLDERSUSER = "folders/account/{username}";
    public static final String FOLDERADD = "folders/{username}";
   //contacts
    public static final String CONTACTSUSER = "contacts/account/{username}";
    public static final String CONTACTADD = "contacts/{username}";
    public static final String CONTACTUPDATE = "contacts/{id}/{username}/";
    //public static final String RULES = "rules";
    public static final String CONTACTSDELETE = "contacts/delete/{id}";
    public static final String RULEADD = "addrule";
    //accaunt
    public static final String ACCOUNTUPDATE = "accounts/update/{id}";
    public static final String ACCOUNTSUSERNAME = "accounts/getallaccount/{username}";


    public static OkHttpClient test() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();
        return client;

    }

    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(SERVICE_API_PATH)
            .addConverterFactory(GsonConverterFactory.create())
            .client(test())
            .build();


    public static AccountService accountService = retrofit.create(AccountService.class);
    public static EmailService emailService = retrofit.create(EmailService.class);
    public static FolderService folderService = retrofit.create(FolderService.class);
    public static ContactService contactService = retrofit.create(ContactService.class);
    public static RuleService ruleService= retrofit.create(RuleService.class);

}

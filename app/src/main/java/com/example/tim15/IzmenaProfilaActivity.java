
package com.example.tim15;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Contact;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ContactService;
import com.example.tim15.Servis.ServiceClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class IzmenaProfilaActivity  extends AppCompatActivity {
    Button button;
    ImageView imageView;
    private static final int PICK_IMAGE=100;
    private static int TIME_OUT = 2000;
    Uri imageUri;
    private EditText username;
    private EditText password;
    private EditText repeatPassword;
    private EditText email;
    public static List<Account> nalozi = new ArrayList<>();
    public static List<Contact> naloziContact = new ArrayList<>();
    private AccountService accountService;
    private ContactService contactService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izmena_profila);
        imageView  =(ImageView) findViewById(R.id.imageView2);
//        button = (Button)findViewById(R.id.IzmenaSlikebtn);
        setTitle("Izmena profila");

        username = (EditText)findViewById(R.id.editText8);
        password = (EditText)findViewById(R.id.editText9);
        repeatPassword = findViewById(R.id.editText6);
        email = (EditText)findViewById(R.id.editText13);


        String UsernameHolder = getIntent().getStringExtra("usernamePrijavljenogKorisnika");


        accountService = ServiceClass.accountService;

        Call call = accountService.getAccounts(UsernameHolder);

        call.enqueue(new Callback<List<Account>>() {
            @Override
            public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
                nalozi = response.body();
                for (Account acc : nalozi){
                    password = (EditText)findViewById(R.id.editText9);
                    password.setText(acc.getPassword());

                }
            }

            @Override
            public void onFailure(Call<List<Account>> call, Throwable t) {
                Log.d("Greska: ", t.getMessage());
            }
        });

        contactService = ServiceClass.contactService;

        Call call1 = contactService.getContacts(UsernameHolder);

        call1.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                naloziContact = response.body();
                for (Contact acc : naloziContact){
                    email = (EditText)findViewById(R.id.editText13);
                    email.setText(acc.getEmail());

                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Log.d("Greska: ", t.getMessage());
            }
        });

        username.setText(UsernameHolder);


        SharedPreferences prefs = getSharedPreferences("prefU", MODE_PRIVATE);
        final String url = prefs.getString("url", "");

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openGallery();
//            }
//        });

//        accountService = ServiceClass.accountService;
//
//        Call call = accountService.getAccounts(UsernameHolder);
//
//        call.enqueue(new Callback<List<Account>>() {
//            @Override
//            public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
//                if (response.isSuccessful()){
//                    nalozi = response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Account>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);

//        ImageView slikaProfila = (ImageView)findViewById(R.id.imageView2);
//        Picasso.get().load(url).into(slikaProfila);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if(id == R.id.action_save){
//
//            String usernameText = username.getText().toString().trim();
//            String passwordText = password.getText().toString().trim();
//            String repeatPasswordText = repeatPassword.getText().toString().trim();
//            String emailText = email.getText().toString().trim();
//
//
//            if (validacija1(usernameText, passwordText, repeatPasswordText, emailText) == true && validacija2(usernameText, passwordText, repeatPasswordText) == true) {
//
//                int idd = getIntent().getIntExtra("idPrijavljenogKorisnika",0);
//                String PhotoHolder = getIntent().getStringExtra("photoPrijavljenogKorisnika");
//
//                Account account = new Account(idd,null, usernameText, passwordText, emailText,PhotoHolder);
//
//                izmenaNaloga(account);
//
//                Snackbar snackbar = Snackbar.make(findViewById(R.id.updateAccount), "Nalog izmenjen! Ulogujte se ponovo!", Snackbar.LENGTH_LONG);
//                snackbar.show();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent intent = new Intent(IzmenaProfilaActivity.this, LoginActivity.class);
//                        intent.putExtra("FromActivityChange", "IzmenaProfilaActivity");
//                        startActivity(intent);
//                        finish();
//                    }
//                }, TIME_OUT);
//
//            } else {
//                //nista
//            }
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void izmenaNaloga(Account izmenaAccount){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        AccountService accountService1 = retrofit1.create(AccountService.class);
        Call<Account> call = accountService1.updateAccount(izmenaAccount.getId(),izmenaAccount);

        call.enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Call<Account> call, Response<Account> response) {
            }

            @Override
            public void onFailure(Call<Account> call, Throwable t) {
                //nista
            }
        });
    }

    public boolean validacija1(String usernameText, String passwordText, String repeatPasswordText, String emailText) {
        if (usernameText.equals("") || passwordText.equals("") || repeatPasswordText.equals("") || emailText.equals("")) {
            android.widget.Toast.makeText(getApplicationContext(), "Morate uneti sva polja!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean validacija2(String usernameText, String passwordText, String repeatPasswordText) {
        if (!Pattern.matches("[a-zA-Z0-9]+",usernameText)) {
            android.widget.Toast.makeText(getApplicationContext(), "Korisnicko ime ne sme sadrzati specijalne karaktere i ne sme imati razmak!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!passwordText.equals(repeatPasswordText)){
            android.widget.Toast.makeText(getApplicationContext(), "Lozinke se ne poklapaju!", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.izmenaprofila, menu);

        return true;
    }
    private void openGallery(){
        Intent gallery =new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode==RESULT_OK && requestCode == PICK_IMAGE){
            imageUri= data.getData();
            imageView.setImageURI(imageUri);

        }

    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}



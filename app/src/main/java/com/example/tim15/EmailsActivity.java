package com.example.tim15;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Email;
import com.example.tim15.Model.SimpleQuery;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.EmailService;
import com.example.tim15.Servis.ServiceClass;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmailsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener  {

    private FloatingActionButton fab;
    private String T="Kreiranje novog mejla";
    public static ListView list;

    public static List<Email> emailovi1= new ArrayList<>();
    public static List<Account> nalozi = new ArrayList<>();
    private static int TIME_OUT = 2000;
    private SharedPreferences sharedPreferences;
    private String userPreferences;
    private String izborPref;
    private String sinhronizacijaPref;
    private long interval;
    private Handler handler;
    private EmailService emailsService;
    private AccountService accountService;
    private EditText search;
    private Button naslovButton;
    private Button posiljalacButton;
    private Button primalacButton;
    private Button sadrzajButton;
    private Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emails);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Primljene poruke");
        handler = new Handler();

        search = findViewById(R.id.editTextSearch);

        sharedPreferences = getSharedPreferences(LoginActivity.Pref,Context.MODE_PRIVATE);
        userPreferences = sharedPreferences.getString(LoginActivity.Username,"");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        izborPref = sharedPreferences.getString("list_preference_2", "1");

        sinhronizacijaPref = sharedPreferences.getString("list_preference_1", "1");

        interval = TimeUnit.MINUTES.toMillis(Integer.parseInt(sinhronizacijaPref));

        fab = (FloatingActionButton)findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.widget.Toast.makeText(getApplicationContext(),T,Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(EmailsActivity.this,CreateEmailActivity.class));
                        Intent intent = new Intent(EmailsActivity.this,CreateEmailActivity.class);
                        intent.putExtra("action","sent");
                        startActivity(intent);
                    }
                }, TIME_OUT);
            }
        });

        list= findViewById(R.id.emailsView);

/*
        emailsService = ServiceClass.emailService;

        Call call = emailsService.getEmails();

        call.enqueue(new Callback<List<Email>>() {
            @Override
            public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                emailovi1 = response.body();
                emailoviProfila1.clear();
                brojac = 0;
                for (Email email : emailovi1){
                    if (email.getTo().getEmail().equals(userPreferences)){
                        emailoviProfila1.add(email);
                        if(email.isProcitana()==false){
                            brojac++;
                            porukaN = email.getSubject();
                            posiljalcN = email.getFrom().getEmail();
                        }
                    }
                }
                if(brojac==1){
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    NotificationCompat.Builder builderN = new NotificationCompat.Builder(EmailsActivity.this,"MyNotification")
                            .setContentText(porukaN)
                            .setContentTitle(posiljalcN)
                            .setSmallIcon(R.drawable.ic_message)
                            .setAutoCancel(true);
                    NotificationManagerCompat nm = NotificationManagerCompat.from(EmailsActivity.this);
                    nm.notify(1,builderN.build());


                }else if(brojac>1){
                    NotificationCompat.Builder builderN = new NotificationCompat.Builder(EmailsActivity.this,"MyNotification")
                            .setContentText("...")
                            .setContentTitle("Imate "+brojac+" nove poruke")
                            .setSmallIcon(R.drawable.ic_pismo_not)
                            .setAutoCancel(true);

                    NotificationManagerCompat nm = NotificationManagerCompat.from(EmailsActivity.this);
                    nm.notify(1,builderN.build());

                }else if(brojac==0){
                    NotificationCompat.Builder builderN = new NotificationCompat.Builder(EmailsActivity.this,"MyNotification")
                            .setContentText("...")
                            .setContentTitle("Imate "+brojac+" nove poruke")
                            .setSmallIcon(R.drawable.ic_nista)
                            .setAutoCancel(true);

                    NotificationManagerCompat nm = NotificationManagerCompat.from(EmailsActivity.this);
                    nm.notify(1,builderN.build());
                    nm.cancel(1);

                }
            }

            @Override
            public void onFailure(Call<List<Email>> call, Throwable t) {
                Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
            }
        });*/
        if (sharedPreferences.getBoolean("test", true) == true) {
            handler.postDelayed(dataLoader, interval);
        }

//---------------Mozda ne treba
/*        for (Email emailP : emailoviProfila) {
            prikaz.add(emailP.getFrom().getEmail()+" "+emailP.getTo().getEmail()+" "+emailP.getSubject()+" "+emailP.getDateTime());
            datum.add(emailP.getDateTime());
            poruka.add(emailP.getContent());
            ime.add(emailP.getFrom().getFirstName());
            prezime.add(emailP.getFrom().getLastName());
            naslov.add(emailP.getSubject());
            emailPosiljalac.add(emailP.getFrom().getEmail());
        }*/

        //--------------------------------------------------------------------

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                Email emailPosiljaoca = new Email();
                                emailPosiljaoca = (Email) (list.getItemAtPosition(position));
                                String emailPosiljaocaText = emailPosiljaoca.getFrom();
                                Date datum = emailPosiljaoca.getDateTime();
                                //Toast.makeText(EmailsActivity.this, String.valueOf(emailovi1.size()), Toast.LENGTH_SHORT).show();
                                for (Email email : emailovi1) {
                                    //Toast.makeText(EmailsActivity.this, stringDatum(email.getDateTime()), Toast.LENGTH_SHORT).show();
                                    if (emailPosiljaoca.equals(email)) {
                                        //if (stringDatum(datum) == stringDatum(email.getDateTime())) {
                                            Email emailSend = email;
                                            Intent intent = new Intent(view.getContext(), EmailActivity.class);
                                            //emailSend.setProcitano(true);

                                            izmenaEmaila(emailSend.getId());
                                            intent.putExtra("email", emailSend);
                                            startActivity(intent);
                                            break;
                                        //}
                                        //else { Toast.makeText(EmailsActivity.this, "Nije proslo if za datum", Toast.LENGTH_SHORT).show(); }


                }
//                else { Toast.makeText(EmailsActivity.this, "Nije proslo if", Toast.LENGTH_SHORT).show(); }
            }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);



        ImageView slikaProfila = (ImageView) headerView.findViewById(R.id.imageView3);
        //Picasso.get().load(url).into(slikaProfila);



        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountService = ServiceClass.accountService;

                Call call = accountService.getAccounts(userPreferences);

                call.enqueue(new Callback<List<Account>>() {
                    @Override
                    public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
                        nalozi = response.body();
                        for (Account acc : nalozi){
                            if (acc.getUsername().equals(userPreferences)){
                                String usernamePrijavljenogKorisnika = acc.getUsername();

                                Intent intent = new Intent(EmailsActivity.this, ProfileActivity.class);
                                intent.putExtra("usernamePrijavljenogKorisnika",usernamePrijavljenogKorisnika);
                                startActivityForResult(intent,acc.getId());
                            }
                        }
                    }
                    @Override
                    public void onFailure (Call < List < Account >> call, Throwable t){
                        Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                    }

                });
            }
        });


        headerView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EmailsActivity.this);

                builder.setTitle("Promena profila");
                builder.setMessage("Da li ste sigurni da zelite da promenite profil?");


                builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        handler.removeCallbacks(dataLoader);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("email", "");
                        editor.commit();
                        Intent intent = new Intent(EmailsActivity.this, LoginActivity.class);
                        intent.putExtra("FromActivityChange","EmailsActivity");
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                return true;    // <- set to true
            }
        });


        SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
        final String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

        TextView email = (TextView)headerView.findViewById(R.id.textView27);

        email.setText(ipAdrs);

        TextView LogOut = (TextView) findViewById(R.id.logout);
        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailsActivity.this, LoginActivity.class);
                intent.putExtra("FromActivityChange","EmailsActivity1");
                startActivity(intent);
                finish();
            }
        });


        toolbar.setLogo(R.drawable.ic_logo);


//  Primer kreiranja JSON objekta za slanje zahteva
//
//        JSONObject json = new JSONObject();
//
//        try {
//            HttpPost post = new HttpPost(URL);
//            json.put("email", email);


        naslovButton = findViewById(R.id.buttonNaslov);

        naslovButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    list = findViewById(R.id.emailsView);

                    emailsService = ServiceClass.emailService;

                    Call call1 = emailsService.reindexEmails();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("subject");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = emailsService.searchEmailPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call2, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call2, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = emailsService.searchEmailTerm(simpleQuery);
                            call.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        posiljalacButton = findViewById(R.id.buttonPosiljalac);

        posiljalacButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.emailsView);

                emailsService = ServiceClass.emailService;

                Call call1 = emailsService.reindexEmails();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("from");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = emailsService.searchEmailPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call2, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call2, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = emailsService.searchEmailTerm(simpleQuery);
                            call.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        primalacButton = findViewById(R.id.buttonPrimalac);

        primalacButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.emailsView);

                emailsService = ServiceClass.emailService;

                Call call1 = emailsService.reindexEmails();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("to");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = emailsService.searchEmailPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call2, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call2, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = emailsService.searchEmailTerm(simpleQuery);
                            call.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        sadrzajButton = findViewById(R.id.buttonSadrzaj);

        sadrzajButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.emailsView);

                emailsService = ServiceClass.emailService;

                Call call1 = emailsService.reindexEmails();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("content");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = emailsService.searchEmailPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call2, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call2, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = emailsService.searchEmailTerm(simpleQuery);
                            call.enqueue(new Callback<List<Email>>() {
                                @Override
                                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                    emailovi1 = response.body();
                                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                                }
                                @Override
                                public void onFailure (Call < List < Email >> call, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        resetButton = findViewById(R.id.buttonReset);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                search.setText("");

                if(izborPref.equals("1")) {
                    //des

                    list= findViewById(R.id.emailsView);

                    emailsService = ServiceClass.emailService;

                    Call call = emailsService.getEmailsDes(userPreferences);

                    call.enqueue(new Callback<List<Email>>() {
                        @Override
                        public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                            emailovi1 = response.body();
                            list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                        }
                        @Override
                        public void onFailure (Call < List < Email >> call, Throwable t){
                            Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                        }

                    });

                }

                if(izborPref.equals("2")){
                    //asc

                    list= findViewById(R.id.emailsView);

                    emailsService = ServiceClass.emailService;

                    Call call = emailsService.getEmailsAsc(userPreferences);

                    call.enqueue(new Callback<List<Email>>() {
                        @Override
                        public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                            emailovi1 = response.body();
                            list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                        }
                        @Override
                        public void onFailure (Call < List < Email >> call, Throwable t){
                            Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                        }

                    });

                }

            }
        });

    }

    public static String stringDatum(Date date) {
        TimeZone tz = TimeZone.getTimeZone("GMT+1");
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public class EmailsAdapter extends ArrayAdapter<Email> {
        private Context mContext;
        int mResource;

        public EmailsAdapter(Context context, int resource, List<Email> objects) {
            super(context, resource, objects);
            mContext = context;
            mResource = resource;
        }



        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
             String email = getItem(position).getFrom();
//            String prezime = getItem(position).getFrom().getLastName();

            String naslov = getItem(position).getSubject();
            String poruka = getItem(position).getContent();
            String datum = stringDatum(getItem(position).getDateTime());
            String emailPosiljaoca = getItem(position).getFrom();
            boolean procitan = getItem(position).isProcitano();

            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            if(procitan==false) {
                ImageView image = convertView.findViewById(R.id.imageEmails);
                TextView xfrom = convertView.findViewById(R.id.fromTXT);
                TextView xnaslov = convertView.findViewById(R.id.naslovEmails);
                TextView xporuka = convertView.findViewById(R.id.porukaEmails);
                TextView xdatum = convertView.findViewById(R.id.datumEmails);
                TextView xemailPosiljaoca = convertView.findViewById(R.id.email);
                TextView xnova = convertView.findViewById(R.id.newMessage);

                xnova.setVisibility(View.VISIBLE);
                xporuka.setTextColor(Color.parseColor("#ff0099cc"));
                xporuka.setTypeface(null, Typeface.BOLD_ITALIC);

                image.setImageResource(R.drawable.pismo);
//                imePrezime.setText(ime + " " + prezime);
                xnaslov.setText(naslov);
                xporuka.setText(poruka);
                xdatum.setText(datum);
                xfrom.setText(email);
                xemailPosiljaoca.setText(emailPosiljaoca);

                return convertView;

            }else if(procitan==true){
                ImageView image = convertView.findViewById(R.id.imageEmails);
                TextView xfrom= convertView.findViewById(R.id.fromTXT);
                TextView xnaslov = convertView.findViewById(R.id.naslovEmails);
                TextView xporuka = convertView.findViewById(R.id.porukaEmails);
                TextView xdatum = convertView.findViewById(R.id.datumEmails);
                TextView xemailPosiljaoca = convertView.findViewById(R.id.email);

                image.setImageResource(R.drawable.pismo);
//                imePrezime.setText(ime + " " + prezime);
                xnaslov.setText(naslov);
                xporuka.setText(poruka);
                xdatum.setText(datum);
                xfrom.setText(email);
                xemailPosiljaoca.setText(emailPosiljaoca);

                return convertView;

            }
            return null;
        }
    }

    Runnable dataLoader = new Runnable() {
        @Override
        public void run() {




                    if(izborPref.equals("1")) {
                        //des

                        list= findViewById(R.id.emailsView);

                        emailsService = ServiceClass.emailService;

                        Call call = emailsService.getEmailsDes(userPreferences);

                        call.enqueue(new Callback<List<Email>>() {
                            @Override
                            public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                emailovi1 = response.body();
                                list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                            }
                                @Override
                                public void onFailure (Call < List < Email >> call, Throwable t){
                                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                        });

                    }

                    if(izborPref.equals("2")){
                        //asc

                        list= findViewById(R.id.emailsView);

                        emailsService = ServiceClass.emailService;

                        Call call = emailsService.getEmailsAsc(userPreferences);

                        call.enqueue(new Callback<List<Email>>() {
                            @Override
                            public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                                emailovi1 = response.body();
                                list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                            }
                            @Override
                            public void onFailure (Call < List < Email >> call, Throwable t){
                                Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                            }

                        });

                    }
                    //list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailoviProfila));
                }

/*            if (sharedPreferences.getBoolean("test", true) == true) {
                handler.postDelayed(dataLoader, interval);
            }*/


    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(EmailsActivity.this);

            builder.setTitle("Log Out");
            builder.setMessage("Da li ste sigurni da zelite da se izlogujete?");


            builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(EmailsActivity.this, LoginActivity.class);
                    intent.putExtra("FromActivityChange","EmailsActivity1");
                    startActivity(intent);
                    finish();
                }
            });

            builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.emails, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            Intent intent = new Intent(EmailsActivity.this,SettingsActivity.class);
            startActivity(intent);

        }


        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_emails) {
            //ss
      } else if (id == R.id.nav_contacts) {
            Intent intent = new Intent(EmailsActivity.this, ContactsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_folders) {
            Intent intent = new Intent(EmailsActivity.this, FoldersActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(EmailsActivity.this,SettingsActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    protected void onStart() {
        super.onStart();
//        dataLoader.run();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() { super.onResume();

        if(izborPref.equals("1")) {
            //des

            list= findViewById(R.id.emailsView);

            emailsService = ServiceClass.emailService;

            Call call = emailsService.getEmailsDes(userPreferences);

            call.enqueue(new Callback<List<Email>>() {
                @Override
                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                    emailovi1 = response.body();
                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                }
                @Override
                public void onFailure (Call < List < Email >> call, Throwable t){
                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                }

            });

        }

        if(izborPref.equals("2")){
            //asc

            list= findViewById(R.id.emailsView);

            emailsService = ServiceClass.emailService;

            Call call = emailsService.getEmailsAsc(userPreferences);

            call.enqueue(new Callback<List<Email>>() {
                @Override
                public void onResponse(Call<List<Email>> call, Response<List<Email>> response) {
                    emailovi1 = response.body();
                    list.setAdapter(new EmailsAdapter(EmailsActivity.this, R.layout.row_emails,emailovi1));

                }
                @Override
                public void onFailure (Call < List < Email >> call, Throwable t){
                    Toast.makeText(EmailsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                }

            });

        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(dataLoader);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(dataLoader);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(dataLoader);
    }




    @Override
    public void onClick(View view) {
    }
    public void izmenaEmaila(int id){

        Call<Void> call = ServiceClass.emailService.viewEmail(id);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                //nista
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
            }
        });
    }
}


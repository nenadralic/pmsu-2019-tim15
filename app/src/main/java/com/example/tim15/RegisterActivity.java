package com.example.tim15;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.User;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ServiceClass;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    private EditText ime;
    private EditText prezime;
    private EditText username;
    private EditText password;
    private EditText REpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ime = (EditText) findViewById(R.id.Imetxt);
        prezime = (EditText) findViewById(R.id.prezimetxt);
        username = (EditText) findViewById(R.id.korisnickoImetxt);
        password = (EditText) findViewById(R.id.lozinkatxt);
        REpassword = (EditText) findViewById(R.id.ponovilozinkutxt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.register, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        String imeText = ime.getText().toString().trim();
        String prezimeText = prezime.getText().toString().trim();
        String usernameText = username.getText().toString().trim();
        String passwordText = password.getText().toString().trim();
        String passwordREText = REpassword.getText().toString().trim();

        int id = item.getItemId();

        if (id == R.id.action_add) {

                kreiranjeUsera(usernameText,passwordText,imeText,prezimeText);

                Account account = new Account();

                account.setUsername(usernameText);
                account.setPassword(passwordText);

                kreiranjeAccounta(account,usernameText);

                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
        }

        return true;

    }


    public void kreiranjeUsera(String username,String password,String ime,String prezime){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        AccountService accountService1 = retrofit1.create(AccountService.class);
        Call<Void> call = accountService1.registrationUser(username,password,ime,prezime);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                //nista
            }
        });
    }

    public void kreiranjeAccounta(Account acc,String username){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        AccountService accountService1 = retrofit1.create(AccountService.class);
        Call<Void> call = accountService1.addAccount(acc,username);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                //nista
            }
        });
    }

    public boolean validacija1(String imeText, String prezimeText, String usernameText,String lozinkaText,String reLozinkaText) {
        if (imeText.equals("") || prezimeText.equals("") || usernameText.equals("")|| lozinkaText.equals("") || reLozinkaText.equals("")) {
            android.widget.Toast.makeText(getApplicationContext(), "Morate uneti sva polja!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
    public boolean validacijaLozinki(String lozinka1,String lozinka2){
        if (!(lozinka1==lozinka2)) {
            android.widget.Toast.makeText(getApplicationContext(), "Lozinke se ne poklapaju!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }





    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

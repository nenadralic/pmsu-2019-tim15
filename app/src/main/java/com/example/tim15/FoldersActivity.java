package com.example.tim15;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Email;
import com.example.tim15.Model.Folder;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.EmailService;
import com.example.tim15.Servis.FolderService;
import com.example.tim15.Servis.ServiceClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoldersActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private FloatingActionButton fab;
    private String T = "Kreiranje novog foldera";
    private ListView list;
    private List<Folder> folderi = new ArrayList<>();
    private List<Folder> folderiPrijavljenog = new ArrayList<>();
    private ArrayList<String> prikaz = new ArrayList<>();
    private ArrayList<String> pnaziv = new ArrayList<>();
    private static int TIME_OUT = 2000;
    int zimage[] = {R.drawable.row_folder_ic, R.drawable.row_folder_ic};
    private FolderService foldersService;
    public static List<Account> nalozi = new ArrayList<>();
    private AccountService accountService;
    private SharedPreferences sharedPreferences;
    private String sinhronizacijaPref;
    private long interval;
    private String userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Folderi");


        final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
        final String ipAdrs = myPrefs.getString(LoginActivity.Username, "");

        sharedPreferences = getSharedPreferences(LoginActivity.Pref,Context.MODE_PRIVATE);
        userPreferences = sharedPreferences.getString(LoginActivity.Username,"");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sinhronizacijaPref = sharedPreferences.getString("list_preference_1", "1");

        interval = TimeUnit.MINUTES.toMillis(Integer.parseInt(sinhronizacijaPref));



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);


        ImageView slikaProfila = (ImageView) headerView.findViewById(R.id.imageView3);
        //Picasso.get().load(url).into(slikaProfila);

        fab = (FloatingActionButton)findViewById(R.id.floatingFolders);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),T,Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(FoldersActivity.this, CreateFolderActivity.class);
                        intent.putExtra("FromActivitySwap","FoldersActivity");
                        startActivity(intent);
                    }
                }, TIME_OUT);

            }
        });


        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountService = ServiceClass.accountService;

                Call call = accountService.getAccounts(userPreferences);

                call.enqueue(new Callback<List<Account>>() {
                    @Override
                    public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
                        nalozi = response.body();
                        for (Account acc : nalozi){
                            if (acc.getUsername().equals(userPreferences)){
                                String usernamePrijavljenogKorisnika = acc.getUsername();

                                Intent intent = new Intent(FoldersActivity.this, ProfileActivity.class);
                                intent.putExtra("usernamePrijavljenogKorisnika",usernamePrijavljenogKorisnika);
                                startActivityForResult(intent,acc.getId());
                            }
                        }
                    }
                    @Override
                    public void onFailure (Call < List < Account >> call, Throwable t){
                        Toast.makeText(FoldersActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                    }

                });
            }
        });

            foldersService = ServiceClass.folderService;

            Call call = foldersService.getFolders(ipAdrs);

        call.enqueue(new Callback<List<Folder>>() {
                @Override
                public void onResponse(Call<List<Folder>> call, Response<List<Folder>> response) {
                    folderi = response.body();
                    list.setAdapter(new MyAdapter(FoldersActivity.this, R.layout.row_folders, folderi));

                }
                @Override
                public void onFailure (Call <List<Folder>> call, Throwable t){
                    Toast.makeText(FoldersActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                }

            });

        headerView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FoldersActivity.this);

                builder.setTitle("Promena profila");
                builder.setMessage("Da li ste sigurni da zelite da promenite profil?");

                builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = myPrefs.edit();
                        editor.putString("email", "");
                        editor.commit();
                        Intent intent = new Intent(FoldersActivity.this, LoginActivity.class);
                        intent.putExtra("FromActivityChange","FoldersActivity");
                        startActivity(intent);


                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                return true;    // <- set to true
            }
        });
        list= (ListView)findViewById(R.id.foldersView);

        for (Folder folder : folderi) {
            prikaz.add(folder.getName());
            pnaziv.add(folder.getName());

        }

       list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Folder folder = new Folder();
                folder = (Folder) (list.getItemAtPosition(position));
                for (Folder folder1 : folderi) {
                    if (folder1.getId()==folder.getId()) {
                        Folder folder2= folder1;
                        Intent intent = new Intent(view.getContext(), FolderActivity.class);
                        intent.putExtra("folder",folder2);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });

        TextView LogOut = (TextView) findViewById(R.id.logout);
        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoldersActivity.this, LoginActivity.class);
                intent.putExtra("FromActivityChange","FoldersActivity1");
                startActivity(intent);
                finish();
            }
        });


        TextView email = (TextView)headerView.findViewById(R.id.textView27);

        email.setText(ipAdrs);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.folders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
                Intent intent = new Intent(FoldersActivity.this,SettingsActivity.class);
                startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    class MyAdapter extends ArrayAdapter<Folder> {

        private Context mContext;
        int mResource;

        MyAdapter(Context context, int resource, List<Folder> objects) {
            super(context, resource, objects);
            this.mContext = context;
            mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            String naziv = getItem(position).getName();

            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);


            ImageView aimage = convertView.findViewById(R.id.image_folders);
            TextView anaziv = convertView.findViewById(R.id.nazivFoldera_txt);

            aimage.setImageResource(R.drawable.row_folder_ic);
            anaziv.setText(naziv);



            return convertView;
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_folders) {
            //ss
        } else if (id == R.id.nav_contacts) {
            Intent intent = new Intent(FoldersActivity.this, ContactsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_emails) {
            Intent intent = new Intent(FoldersActivity.this, EmailsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
                Intent intent = new Intent(FoldersActivity.this,SettingsActivity.class);
                startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

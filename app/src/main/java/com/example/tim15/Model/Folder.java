package com.example.tim15.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Folder implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("folders")
    @Expose
    private ArrayList<Folder> folders;
    @SerializedName("rule")
    @Expose
    private Rule rule;
    @SerializedName("messages")
    @Expose
    private ArrayList<Email> messages;


    public Folder() {
    }

    public Folder(int id, String name, ArrayList<Folder> folders, Rule rule, ArrayList<Email> messages) {
        this.id = id;
        this.name = name;
        this.folders = folders;
        this.rule = rule;
        this.messages = messages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Folder> getFolders() {
        return folders;
    }

    public void setFolders(ArrayList<Folder> folders) {
        this.folders = folders;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public ArrayList<Email> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Email> messages) {
        this.messages = messages;
    }
}




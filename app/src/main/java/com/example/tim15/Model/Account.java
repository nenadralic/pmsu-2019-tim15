package com.example.tim15.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Account implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("smtpAddress")
    @Expose
    private String smtpAddress;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("messages")
    @Expose
    private ArrayList<Email> messages;
    @SerializedName("pop3Imap")
    @Expose
    private String pop3Imap;
    @SerializedName("displayName")
    @Expose
    private String displayName;





    public Account() {
    }

    public Account(int id, String smtpAddress, String username, String password, ArrayList<Email> messages, String pop3Imap, String displayName) {
        this.id = id;
        this.smtpAddress = smtpAddress;
        this.username = username;
        this.password = password;
        this.messages = messages;
        this.pop3Imap = pop3Imap;
        this.displayName = displayName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmtpAddress() {
        return smtpAddress;
    }

    public void setSmtpAddress(String smtpAddress) {
        this.smtpAddress = smtpAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Email> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Email> messages) {
        this.messages = messages;
    }

    public String getPop3Imap() {
        return pop3Imap;
    }

    public void setPop3Imap(String pop3Imap) {
        this.pop3Imap = pop3Imap;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayNamedisplayName(String displayName) {
        this.displayName = displayName;
    }
}

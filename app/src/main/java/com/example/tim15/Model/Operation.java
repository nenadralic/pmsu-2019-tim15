package com.example.tim15.Model;

import java.io.Serializable;

public enum Operation implements Serializable {
    MOVE,
    COPY,
    DELETE;

    public String getStatus() {
        return this.name();
    }
}

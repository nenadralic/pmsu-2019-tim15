package com.example.tim15;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Contact;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ContactService;
import com.example.tim15.Servis.ServiceClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AccountService accountService;
    private String password;
    private int idd;
    private String photo;
    public static List<Account> accounts = new ArrayList<>();
    public static List<Account> nalog = new ArrayList<>();
    public static List<Contact> nalogContact = new ArrayList<>();
    private ContactService contactService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Profil");

        String EmailHolder = getIntent().getStringExtra("emailPrijavljenogKorisnika");


        SharedPreferences prefs = getSharedPreferences("prefU", MODE_PRIVATE);
//        final String url = prefs.getString("url", "");
        final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
//        final String ipAdrs=myPrefs.getString(LoginActivity.Email, "");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationViewH = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationViewH.getHeaderView(0);

        TextView email = (TextView)headerView.findViewById(R.id.textView27);

        TextView emailK = (TextView)findViewById(R.id.emailTxtProfil);
        TextView username = (TextView)findViewById(R.id.usernameProfil);
        String UsernameHolder = getIntent().getStringExtra("usernamePrijavljenogKorisnika");

//        emailK.setText(ipAdrs);
        email.setText(UsernameHolder);
        username.setText(UsernameHolder);

        contactService = ServiceClass.contactService;

        Call call1 = contactService.getContacts(UsernameHolder);

        call1.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                nalogContact = response.body();
                for (Contact acc : nalogContact){
                    TextView emailK = (TextView)findViewById(R.id.emailTxtProfil);
                    emailK.setText(acc.getEmail());

                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Log.d("Greska: ", t.getMessage());
            }
        });
        TextView LogOut = (TextView) findViewById(R.id.logout);
        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                intent.putExtra("FromActivityChange","ProfileActivity1");
                startActivity(intent);
                finish();
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        ImageView slikaProfila = (ImageView) headerView.findViewById(R.id.imageView3);
//        Picasso.get().load(url).into(slikaProfila);
//
//        ImageView slikaProfila2 = (ImageView) findViewById(R.id.imageView3);
//        Picasso.get().load(url).into(slikaProfila2);

        headerView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);

                builder.setTitle("Promena profila");
                builder.setMessage("Da li ste sigurni da zelite da promenite profil?");

                builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
//                        SharedPreferences.Editor editor = myPrefs.edit();
//                        editor.putString("email", "");
//                        editor.commit();
                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                        intent.putExtra("FromActivityChange","ProfileActivity");
                        startActivity(intent);
                        finish();

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                return true;    // <- set to true
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(ProfileActivity.this,SettingsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_edit) {
            String UsernameHolder = getIntent().getStringExtra("usernamePrijavljenogKorisnika");

            Intent intent = new Intent(ProfileActivity.this,IzmenaProfilaActivity.class);
            intent.putExtra("usernamePrijavljenogKorisnika",UsernameHolder);
            startActivity(intent);

        }
        else if(id == R.id.action_change_profile){
            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
            intent.putExtra("FromActivityChange","ProfileActivity");
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_folders) {
            Intent intent = new Intent(ProfileActivity.this, FoldersActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_contacts) {
            Intent intent = new Intent(ProfileActivity.this, ContactsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_emails) {
            Intent intent = new Intent(ProfileActivity.this, EmailsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(ProfileActivity.this,SettingsActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}


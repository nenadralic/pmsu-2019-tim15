package com.example.tim15;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.tim15.Model.Account;
import com.example.tim15.Model.Condition;
import com.example.tim15.Model.Folder;
import com.example.tim15.Model.Operation;
import com.example.tim15.Model.Rule;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.FolderService;
import com.example.tim15.Servis.RuleService;
import com.example.tim15.Servis.ServiceClass;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateFolderActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static int TIME_OUT = 2000;

    private EditText nazivFoldera;

    public String activity;
//---------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent =getIntent();
        Folder folder =(Folder) intent.getSerializableExtra("folder");
        activity = getIntent().getStringExtra("FromActivitySwap");

        if(activity.equals("Folder2Activity")) {
            setTitle("Izmena foldera");
        }else if(activity.equals("FoldersActivity")){
            setTitle("Dodavanje foldera");
        }




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);


/*        //Definisanje spinnera za combobox Condition-----------------------------------------------------------
        spiner = (Spinner) findViewById(R.id.cbCondition);
        spiner.setAdapter(new ArrayAdapter<Condition>(this, android.R.layout.simple_spinner_dropdown_item, Condition.values()));
        spiner.setOnItemSelectedListener(this);
        //-------------------------------------------------------------------------------------------------------------

        //Definisanje spinnera za combobox Operation
        spiner1 = (Spinner) findViewById(R.id.cbOperation);
        spiner1.setAdapter(new ArrayAdapter<Operation>(this, android.R.layout.simple_spinner_dropdown_item, Operation.values()));
        spiner1.setOnItemSelectedListener(this);
        //-------------------------------------------------------------------------------------------------------------


        if (activity.equals("Folder2Activity")){
            nazivFoldera.setText(folder.getName());
            if(folder.getDestination()!=null) {
                spiner.setSelection(getIndex(spiner, folder.getDestination().getCondition().toString()));
                spiner1.setSelection(getIndex(spiner1, folder.getDestination().getOperation().toString()));
            }
            }*/

        //-----------------------------------------------------------------------------------------------------------
        //DOBAVLJANJE LISTE SVIH Rules-a SA SERVISA--------------------------------------------------------
/*        ruleService = ServiceClass.ruleService;

        Call call2 = ruleService.getRules();

        call2.enqueue(new Callback<List<Rule>>() {
            @Override
            public void onResponse(Call<List<Rule>> call2, Response<List<Rule>> response) {
                if (response.isSuccessful()) {
                    rules = response.body();
                }
            }

            @Override
            public void onFailure(Call<List<Rule>> call2, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
        //-----------------------------------------------------------------------------------------------------------
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_folder, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            startActivity(new Intent(CreateFolderActivity.this, SettingsActivity.class));

        }
        if (id == R.id.action_save) {
/*            if (activity.equals("Folder2Activity")) {

                String naziv = nazivFoldera.getText().toString().trim();
                String condition = spiner.getSelectedItem().toString();
                String operation = spiner1.getSelectedItem().toString();
                if (validacija1(naziv, condition, operation) == true) {
                    Operation op = Operation.valueOf(operation);
                    Condition con = Condition.valueOf(condition);

                    Intent intent =getIntent();
                    Folder folder1 =(Folder) intent.getSerializableExtra("folder");
                    int brojacRules = 0;
                    for (Rule rule : rules) {
                        if (rule.getId() > brojacRules) {
                            brojacRules = rule.getId();
                        }
                    }

                    Rule rule = new Rule(brojacRules + 1, con, op, null);

                    kreirajRule(rule);

                    rules.add(rule);

                    Folder folder = new Folder();
                    folder.setId(folder1.getId());
                    folder.setName(naziv);
                    folder.setDestination(rule);
                    folder.setNadfolder(folder1.getNadfolder());
                    folder.setAcc(folder1.getAcc());


                    izmenaFoldera(folder);

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.createFolder), "Folder izmenjen!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(CreateFolderActivity.this, FoldersActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME_OUT);

                } else {
                }
            }
            else */
                if (activity.equals("FoldersActivity")) {

                nazivFoldera= (EditText)findViewById(R.id.txtNazivFoldera);

                String naziv = nazivFoldera.getText().toString().trim();

                    SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
                    String ipAdrs = myPrefs.getString(LoginActivity.Username, "");

                    Folder folder = new Folder();
                    folder.setName(naziv);
                    kreirajFolder(folder, ipAdrs);

                    Snackbar snackbar = Snackbar.make(findViewById(R.id.createFolder), "Kreiran folder!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(CreateFolderActivity.this, FoldersActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }, TIME_OUT);

            }
        }
        if(id == R.id.action_cancel){
            startActivity(new Intent(CreateFolderActivity.this,FoldersActivity.class));
        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    //Definisanje klika na combobox------------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(),text,Toast.LENGTH_LONG);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    //----------------------------------------------------------------------------------
//    public void izmenaFoldera(Folder izmenaFoldera){
//        Retrofit retrofit1 = new Retrofit.Builder()
//                .baseUrl(ServiceClass.SERVICE_API_PATH)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(ServiceClass.test())
//                .build();
//
//        FolderService folderService3= retrofit1.create(FolderService.class);
//        Call<Folder> call = folderService3.updateFolder(izmenaFoldera.getId(),izmenaFoldera);
//
//        call.enqueue(new Callback<Folder>() {
//            @Override
//            public void onResponse(Call<Folder> call, Response<Folder> response) {
//            }
//
//            @Override
//            public void onFailure(Call<Folder> call, Throwable t) {
//                //nista
//            }
//        });
//    }
    public void kreirajFolder(Folder createFolder, String username){
        Retrofit retrofit5 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        FolderService folderService5 = retrofit5.create(FolderService.class);
        Call<Folder> call5 = folderService5.createFolder(createFolder, username);

        call5.enqueue(new Callback<Folder>() {
            @Override
            public void onResponse(Call<Folder> call5, Response<Folder> response) {
            }

            @Override
            public void onFailure(Call<Folder> call5, Throwable t) {
            }
        });
    }
    //----------------------------------------------------------------------------------
    public void kreirajRule(Rule createRule){
        Retrofit retrofit4 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        RuleService ruleService4 = retrofit4.create(RuleService.class);
        Call<Rule> call4 = ruleService4.createRule(createRule);

        call4.enqueue(new Callback<Rule>() {
            @Override
            public void onResponse(Call<Rule> call4, Response<Rule> response) {
            }

            @Override
            public void onFailure(Call<Rule> call4, Throwable t) {
            }
        });
    }

    public void onBackPressed() {
        super.onBackPressed();
    }


    private int getIndex(Spinner spiner1, String con){
        for (int i=0;i<spiner1.getCount();i++){
            if (spiner1.getItemAtPosition(i).toString().equalsIgnoreCase(con)){
                return i;
            }
        }

        return 0;
    }


}

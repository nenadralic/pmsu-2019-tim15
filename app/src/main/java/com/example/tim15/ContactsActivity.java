package com.example.tim15;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tim15.Model.Account;
import com.example.tim15.Model.Contact;
import com.example.tim15.Model.SimpleQuery;
import com.example.tim15.Servis.AccountService;
import com.example.tim15.Servis.ContactService;
import com.example.tim15.Servis.ServiceClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,NavigationView.OnClickListener {
    private FloatingActionButton fab;
    private String T="Kreiranje novog kontakta";
    public static ListView list;
    private List<Contact> kontakti = new ArrayList<>();
    private List<Contact> kontaktiLista = new ArrayList<>();
    private ArrayList<String> prikaz= new ArrayList<>();
    private ArrayList<String> ime = new ArrayList<>();
    private ArrayList<String> prezime = new ArrayList<>();
    private static int TIME_OUT = 2000;
    private SharedPreferences sharedPreferences;
    public static List<Account> nalozi = new ArrayList<>();
    private ContactService contactsService;
    private AccountService accountService;
    private Account prijavljen;
    private String userPreferences;
    private EditText search;
    private Button imeButton;
    private Button prezimeButton;
    private Button napomenaButton;
    private Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Kontakti");

        search = findViewById(R.id.searchContact);

        final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
        final String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

        sharedPreferences = getSharedPreferences(LoginActivity.Pref,Context.MODE_PRIVATE);
        userPreferences = sharedPreferences.getString(LoginActivity.Username,"");

        contactsService = ServiceClass.contactService;

        Call call = contactsService.getContacts(ipAdrs);

        call.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                kontakti = response.body();
                list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

            }
            @Override
            public void onFailure (Call <List<Contact>> call, Throwable t){
                Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
            }

        });

        SharedPreferences prefs = getSharedPreferences("prefU", MODE_PRIVATE);
        final String url = prefs.getString("url", "");


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        fab = (FloatingActionButton)findViewById(R.id.floatContacts);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.widget.Toast.makeText(getApplicationContext(),T, Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(ContactsActivity.this, CreateContactActivity.class);
                        intent.putExtra("FromActivitySwap","ContactsActivity");
                        startActivity(intent);

                    }
                }, TIME_OUT);

            }
        });



        list= (ListView)findViewById(R.id.contactsView);


    //    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts, kontaktiLista));

        for (Contact kontakt : kontakti) {
            prikaz.add(kontakt.getFirstName()+" "+kontakt.getLastName());
            ime.add(kontakt.getFirstName());
            prezime.add(kontakt.getLastName());
        }

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact= new Contact();
                contact = (Contact) (list.getItemAtPosition(position));
                for (Contact contact1 : kontakti) {
                    if (contact1.equals(contact)) {
                        Contact contact2= contact1;
                        Intent intent = new Intent(view.getContext(), ContactActivity.class);
                        intent.putExtra("contact",contact2);
                        startActivity(intent);
                        break;
                    }
                }
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

//        ImageView slikaProfila = (ImageView) headerView.findViewById(R.id.imageView3);
//        Picasso.get().load(url).into(slikaProfila);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountService = ServiceClass.accountService;

                Call call = accountService.getAccounts(userPreferences);

                call.enqueue(new Callback<List<Account>>() {
                    @Override
                    public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
                        nalozi = response.body();
                        for (Account acc : nalozi){
                            if (acc.getUsername().equals(userPreferences)){
                                String usernamePrijavljenogKorisnika = acc.getUsername();

                                Intent intent = new Intent(ContactsActivity.this, ProfileActivity.class);
                                intent.putExtra("usernamePrijavljenogKorisnika",usernamePrijavljenogKorisnika);
                                startActivityForResult(intent,acc.getId());
                            }
                        }
                    }
                    @Override
                    public void onFailure (Call < List < Account >> call, Throwable t){
                        Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                    }

                });
            }
        });

        headerView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ContactsActivity.this);

                builder.setTitle("Promena profila");
                builder.setMessage("Da li ste sigurni da zelite da promenite profil?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = myPrefs.edit();
                        editor.putString("email", "");
                        editor.commit();
                        Intent intent = new Intent(ContactsActivity.this, LoginActivity.class);
                        intent.putExtra("FromActivityChange","ContactsActivity");
                        startActivity(intent);
                        finish();

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                return true;    // <- set to true
            }
        });


        TextView email = (TextView)headerView.findViewById(R.id.textView27);

        email.setText(ipAdrs);

        TextView LogOut = (TextView) findViewById(R.id.logout);
        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsActivity.this, LoginActivity.class);
                intent.putExtra("FromActivityChange","ContactsActivity1");
                startActivity(intent);
                finish();
            }
        });


        imeButton = findViewById(R.id.buttonIme);

        imeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.contactsView);

                contactsService = ServiceClass.contactService;

                Call call1 = contactsService.reindexContacts();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("firstName");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = contactsService.searchContactPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call2, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call2, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = contactsService.searchContactTerm(simpleQuery);
                            call.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        prezimeButton = findViewById(R.id.buttonPrezime);

        prezimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.contactsView);

                contactsService = ServiceClass.contactService;

                Call call1 = contactsService.reindexContacts();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("lastName");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = contactsService.searchContactPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call2, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call2, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = contactsService.searchContactTerm(simpleQuery);
                            call.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        napomenaButton = findViewById(R.id.buttonNapomena);

        napomenaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list = findViewById(R.id.contactsView);

                contactsService = ServiceClass.contactService;

                Call call1 = contactsService.reindexContacts();

                call1.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call1, Response<Void> response) {
                        SimpleQuery simpleQuery = new SimpleQuery();

                        String searchTerm = search.getText().toString().trim();

                        simpleQuery.setField("note");
                        simpleQuery.setValue(searchTerm);

                        if(searchTerm.trim().indexOf(' ') >= 0) {

                            Call call2 = contactsService.searchContactPhrase(simpleQuery);
                            call2.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call2, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call2, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });

                        } else {
                            Call call = contactsService.searchContactTerm(simpleQuery);
                            call.enqueue(new Callback<List<Contact>>() {
                                @Override
                                public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                                    kontakti = response.body();
                                    list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                                }
                                @Override
                                public void onFailure (Call < List < Contact >> call, Throwable t){
                                    Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }


                    }

                    @Override
                    public void onFailure(Call<Void> call1, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        resetButton = findViewById(R.id.buttonResetContact);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                search.setText("");

                contactsService = ServiceClass.contactService;

                Call call = contactsService.getContacts(ipAdrs);

                call.enqueue(new Callback<List<Contact>>() {
                    @Override
                    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                        kontakti = response.body();
                        list.setAdapter(new MyAdapter(ContactsActivity.this, R.layout.row_contacts,kontakti));

                    }
                    @Override
                    public void onFailure (Call <List<Contact>> call, Throwable t){
                        Toast.makeText(ContactsActivity.this, "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                    }

                });

            }
        });


    }

    public class MyAdapter extends ArrayAdapter<Contact>{

        private Context mContext;
        int mResource;

        MyAdapter(Context context, int resource, List<Contact> objects) {
            super(context, resource, objects);
            this.mContext = context;
            mResource = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            String ime = getItem(position).getFirstName();
            String prezime = getItem(position).getLastName();
            String email = getItem(position).getEmail();

            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

            ImageView aimage = convertView.findViewById(R.id.imageContacts);
            TextView aime = convertView.findViewById(R.id.contactFirstName);
            TextView emailContact = convertView.findViewById(R.id.emailContact);

            aimage.setImageResource(R.drawable.ic_contact_image);
            aime.setText(ime + " " + prezime);
            emailContact.setText(email);

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            Intent intent = new Intent(ContactsActivity.this,SettingsActivity.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_emails) {
            Intent intent = new Intent(ContactsActivity.this, EmailsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_contacts) {
          //nis
        } else if (id == R.id.nav_folders) {
            Intent intent = new Intent(ContactsActivity.this, FoldersActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(ContactsActivity.this,SettingsActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void onClick(View view) {
        if(view.getId() == R.id.imageView) {
            //startActivity(new Intent(ContactsActivity.this, ProfileActivity.class));
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
